package com.sisa.qrjava;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.camera.core.Camera;
import androidx.camera.core.CameraSelector;
import androidx.camera.core.ImageAnalysis;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.ImageProxy;
import androidx.camera.core.Preview;
import androidx.camera.lifecycle.ProcessCameraProvider;
import androidx.camera.view.PreviewView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.LifecycleOwner;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.Task;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.mlkit.vision.barcode.BarcodeScanner;
import com.google.mlkit.vision.barcode.BarcodeScannerOptions;
import com.google.mlkit.vision.barcode.BarcodeScanning;
import com.google.mlkit.vision.barcode.common.Barcode;
import com.google.mlkit.vision.common.InputImage;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;


public class MainActivity extends AppCompatActivity {
    int CAMERA_PERMISSION_REQUEST_CODE = 1;
    int STORAGE_PERMISSION = 0x10;
    private static int RESULT_LOAD_IMAGE = 8;


    private ImageCapture imageCapture;
    PreviewView cameraQr;
    TextView text;
    Button buttonGalery;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        cameraQr = findViewById(R.id.cameraView);
        text = findViewById(R.id.bottomText);
        buttonGalery = findViewById(R.id.galeria);

        buttonGalery.setOnClickListener(view -> {
            if (verifyPermissionStorage()) {
                openGallery();
            } else {
                requestPermissionStorage();
            }
        });


        if (hasCameraPermission()) bindCameraUseCases();
        else requestPermissionCamera();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == CAMERA_PERMISSION_REQUEST_CODE
                && grantResults[0] == PackageManager.PERMISSION_GRANTED
        ) {
            // user granted permissions - we can set up our scanner
            bindCameraUseCases();
        }

        if (requestCode == STORAGE_PERMISSION && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            openGallery();
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();

            InputImage image;
            try {
                image = InputImage.fromFilePath(this, selectedImage);
                scanImageGallery(image);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    private boolean hasCameraPermission() {
        return ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermissionCamera() {
        ActivityCompat.requestPermissions(
                this,
                new String[]{Manifest.permission.CAMERA
                }, CAMERA_PERMISSION_REQUEST_CODE);
    }

    private void requestPermissionStorage() {
        ActivityCompat.requestPermissions(
                this,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                STORAGE_PERMISSION);
    }

    private boolean verifyPermissionStorage() {
        return ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    void openGallery() {
        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, RESULT_LOAD_IMAGE);
    }

    private void bindCameraUseCases() {
        ListenableFuture<ProcessCameraProvider> cameraProviderFuture =
                ProcessCameraProvider.getInstance(this);
        cameraProviderFuture.addListener(() -> {
            try {
                // Camera provider is now guaranteed to be available
                ProcessCameraProvider cameraProvider = cameraProviderFuture.get();

                // Set up the view finder use case to display camera preview
                Preview preview = new Preview.Builder().build();

                // Set up the capture use case to allow users to take photos
                imageCapture = new ImageCapture.Builder()
                        .setCaptureMode(ImageCapture.CAPTURE_MODE_MINIMIZE_LATENCY)
                        .build();

                // Choose the camera by requiring a lens facing
                CameraSelector cameraSelector = new CameraSelector.Builder()
                        .requireLensFacing(CameraSelector.LENS_FACING_BACK)
                        .build();

                ImageAnalysis analysisUseCase = new ImageAnalysis.Builder()
                        .setBackpressureStrategy(ImageAnalysis.STRATEGY_KEEP_ONLY_LATEST)
                        .build();
                analysisUseCase.setAnalyzer(
                        Executors.newSingleThreadExecutor(),
                        image -> {
                            scanBarcodes(image);
                        }
                );

                // Attach use cases to the camera with the same lifecycle owner
                Camera camera = cameraProvider.bindToLifecycle(
                        ((LifecycleOwner) this),
                        cameraSelector,
                        preview,
                        analysisUseCase);


                // Connect the preview use case to the previewView
                preview.setSurfaceProvider(
                        cameraQr.getSurfaceProvider());
            } catch (InterruptedException | ExecutionException e) {
                // Currently no exceptions thrown. cameraProviderFuture.get()
                // shouldn't block since the listener is being called, so no need to
                // handle InterruptedException.
            }
        }, ContextCompat.getMainExecutor(this));

    }


    private void scanBarcodes(ImageProxy image) {
        // [START set_detector_options]
        BarcodeScannerOptions options =
                new BarcodeScannerOptions.Builder()
                        .setBarcodeFormats(
                                Barcode.FORMAT_QR_CODE,
                                Barcode.FORMAT_AZTEC)
                        .build();
        // [END set_detector_options]

        // [START get_detector]
//        BarcodeScanner scanner = BarcodeScanning.getClient();
        // Or, to specify the formats to recognize:
        BarcodeScanner scanner = BarcodeScanning.getClient(options);
        // [END get_detector]
        @SuppressLint("UnsafeOptInUsageError") InputImage inputImage = InputImage.fromMediaImage(Objects.requireNonNull(image.getImage()), image.getImageInfo().getRotationDegrees());

        // [START run_detector]
        @SuppressLint("UnsafeOptInUsageError") Task<List<Barcode>> result = scanner.process(inputImage)
                .addOnSuccessListener(barcodes -> {
                    // Task completed successfully
                    // [START_EXCLUDE]
                    // [START get_barcodes]
                    for (Barcode barcode : barcodes) {
                        Rect bounds = barcode.getBoundingBox();
                        Point[] corners = barcode.getCornerPoints();

                        String rawValue = barcode.getRawValue();
                        Log.d("VALUE", "valor qr camara -------> " + rawValue);

                    }
                    // [END get_barcodes]
                    // [END_EXCLUDE]
                })
                .addOnFailureListener(e -> {
                    // Task failed with an exception
                    Log.d("VALUE", "valor qr -------> " + e);

                    // ...
                })
                .addOnCompleteListener((f) -> {
                    image.getImage().close();
                    image.close();
                });
        // [END run_detector]
    }

    private void scanImageGallery(InputImage inputImage) {
        // [START set_detector_options]
        BarcodeScannerOptions options =
                new BarcodeScannerOptions.Builder()
                        .setBarcodeFormats(
                                Barcode.FORMAT_QR_CODE,
                                Barcode.FORMAT_AZTEC)
                        .build();
        // [END set_detector_options]

        // [START get_detector]
//        BarcodeScanner scanner = BarcodeScanning.getClient();
        // Or, to specify the formats to recognize:
        BarcodeScanner scanner = BarcodeScanning.getClient(options);
        // [END get_detector]

        // [START run_detector]
        @SuppressLint("UnsafeOptInUsageError") Task<List<Barcode>> result = scanner.process(inputImage)
                .addOnSuccessListener(barcodes -> {
                    // Task completed successfully
                    // [START_EXCLUDE]
                    // [START get_barcodes]
                    for (Barcode barcode : barcodes) {
                        Rect bounds = barcode.getBoundingBox();
                        Point[] corners = barcode.getCornerPoints();

                        String rawValue = barcode.getRawValue();
                        Log.d("VALUE", "valor qr caleria -------> " + rawValue);

                    }
                    // [END get_barcodes]
                    // [END_EXCLUDE]
                })
                .addOnFailureListener(e -> {
                    // Task failed with an exception
                    Log.d("VALUE", "valor qr -------> " + e);

                    // ...
                })
                .addOnCompleteListener((f) -> {
                    f.isComplete();
                });
        // [END run_detector]
    }


    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {
        super.onPointerCaptureChanged(hasCapture);
    }
}